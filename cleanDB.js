const mysql = require('mysql2')
const parseConf = function (config) {
  if (config) {
    var aux = config.split(';').map(function (element) {
      return element.split('=')
    })
    return {
      host: aux[1][1].split(':')[0],
      port: aux[1][1].split(':')[1],
      user: aux[2][1],
      password: aux[3][1],
      database: aux[0][1]
    }
  }
  return null
}
const config = parseConf(process.env.MYSQLCONNSTR_localdb) || {
  'host': 'localhost',
  'user': 'root',
  'password': 'skypunch',
  'database': 'rifaT'
}

const connection = mysql.createConnection(config)

connection.connect()
connection.query('DELETE FROM rifa_F94_01', function (err, rows) {
  if (err) console.error(err)
})
connection.end()
console.log('Boletos Eliminados')
