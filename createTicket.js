const randomString = require('./helpers/RandomString')
const mysql = require('mysql2')

const parseConf = function (config) {
  if (config) {
    let aux = config.split(';').map((element) => element.split('='))
    return {
      host: aux[1][1].split(':')[0],
      port: aux[1][1].split(':')[1],
      user: aux[2][1],
      password: aux[3][1],
      database: aux[0][1]
    }
  }
  return null
}
const config = parseConf(process.env.MYSQLCONNSTR_localdb) || {
  'host': 'localhost',
  'user': 'root',
  'password': 'skypunch',
  'database': 'rifaT'
}

const connection = mysql.createConnection(config)

connection.connect()
for (let i = 0; i < 200; i++) {
  let query = `INSERT INTO rifa_F94_01 (folio) values('${randomString(8)}')`
  connection.query(query, function (err, rows) {
    if (err) console.error(i, err)
  })
}
connection.end()
console.log('Boletos Creados')
