const crypto = require('crypto')
module.exports = (length = 16, format = 'hex') => {
  return crypto.randomBytes(length).toString(format)
}
