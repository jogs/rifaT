/*
** Module dependencies
*/
const express = require('express')
const emailjs = require('emailjs')
const helmet = require('helmet')
const mysql = require('mysql2')
const bodyParser = require('body-parser')

const parseConf = (config) => {
  if (config) {
    let aux = config.split(';').map((element) => element.split('='))
    return {
      host: aux[1][1].split(':')[0],
      port: aux[1][1].split(':')[1],
      user: aux[2][1],
      password: aux[3][1],
      database: aux[0][1]
    }
  }
  return null
}

const PORT = process.env.port || 8080
const config = parseConf(process.env.MYSQLCONNSTR_localdb) || require('./config.dev.json')
const emailConfig = require('./email.json')

const email = emailjs.server.connect(emailConfig)
const app = express()
app.use(bodyParser.json())
app.use(helmet())
app.use(helmet.hidePoweredBy({ setTo: 'Skypunch' }))
app.use(express.static(`${__dirname}/public`))

app.use((req, res, next) => {
  console.log(`${req.method}: ${req.path}
    at ${Date()}`)
  res.header('Access-Control-Allow-Origin', '*')
  res.header('Access-Control-Allow-Methods', 'GET,POST,OPTIONS,PUT,DELETE')
  res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization, Content-Length, Origin, X-Requested-With, Accept, From, User-Agent')
  if (req.method === 'OPTIONS') {
    res.status(200)
    .json({})
  } else {
    next()
  }
})

app.route('/')
.get((req, res) => {
  res.status(200)
  .type('html')
  .sendFile(`${__dirname}/public/index.html`)
})
app.route('/reset/all')
.put((req, res) => {
  let randomString = require('./helpers/RandomString')
  let remove = mysql.createConnection(config)
  remove.connect()
  remove.query('DELETE FROM rifa_F94_01')
  remove.end()
  let create = mysql.createConnection(config)
  create.connect()
  for (let i = 0; i < 200; i++) {
    create.query(`INSERT INTO rifa_F94_01 (folio) values('${randomString(8)}')`)
  }
  create.end()
  res.status(200)
  .type('text')
  .send('ok')
})
app.route('/resumen')
.get((req, res) => {
  var select = mysql.createConnection(config)
  select.connect()
  select.query(`SELECT count(folio) as 'Comprados', no_cuenta FROM rifa_F94_01 GROUP BY no_cuenta`, (err, rows) => {
    if (err) {
      console.error(err)
      res.status(400)
      .json({
        error: err
      })
    } else {
      var count = mysql.createConnection(config)
      count.connect()
      count.query(`SELECT count(folio) as 'total' FROM rifa_F94_01 WHERE activo=1`, (error, number) => {
        if (err) {
          console.error(error)
          res.status(400)
          .json({
            error: error
          })
        } else {
          res.status(200)
          .type('json')
          .json({
            success: 'Ok',
            body: {
              resumen: rows,
              total: number
            }
          })
        }
      })
      count.end()
    }
  })
  select.end()
})
app.route('/boleto/no_cuenta/:no_cuenta')
.get((req, res) => {
  var select = mysql.createConnection(config)
  select.connect()
  select.query(`SELECT count(folio) as 'Comprados', no_cuenta FROM rifa_F94_01 WHERE no_cuenta='${req.params.no_cuenta}' GROUP BY no_cuenta LIMIT 1`, (err, rows, fields) => {
    if (err) {
      console.error(err)
      res.status(400)
      .json({
        error: err
      })
    } else {
      res.status(200)
      .type('json')
      .json({
        success: 'Ok',
        body: rows || {}
      })
    }
  })
  select.end()
})
app.route('/boleto')
.get((req, res) => {
  var select = mysql.createConnection(config)
  select.connect()
  select.query('SELECT folio FROM rifa_F94_01 WHERE activo=0', (err, rows, fields) => {
    if (err) {
      console.error(err)
      res.status(400)
      .json({
        error: err
      })
    } else {
      let list = 'Lista de folios'
      for (let element of rows) {
        list += `
          folio: ${element.folio}`
      }
      email.send({
        text: list,
        from: 'No Reply <no_reply_skypunch@outlook.com>',
        to: `Joaquín García Santiago <the.jogs.1917@gmail.com>`,
        subject: 'Lista de Folios'
      }, (err, message) => {
        res.status(200)
        .type('json')
        .json({
          success: 'Ok',
          body: rows.length || 0,
          email: err || 'Lista enviada al administrador'
        })
      })
    }
  })
})
.post((req, res) => {
  if (req.is('json')) {
    var select = mysql.createConnection(config)
    select.connect()
    select.query(`SELECT folio FROM rifa_F94_01 WHERE folio='${req.body.folio}' and activo=0 limit 1`, (error, rows, fields) => {
      if (error) {
        console.error(error)
        res.status(400)
        .json({
          error: error
        })
      } else {
        if (rows.length > 0) {
          var query = `UPDATE rifa_F94_01 SET activo=true, nombre='${req.body.nombre}', no_cuenta='${req.body.no_cuenta}', email='${req.body.email}' WHERE folio='${req.body.folio}' limit 1`
          // console.log(query)
          var update = mysql.createConnection(config)
          update.query(query, (err, payload) => {
            if (err) {
              console.error(err)
              res.status(400)
              .json({
                error: err
              })
            } else {
              // console.log(payload || {})
              email.send({
                text: `Se registro el folio ${req.body.folio}
                para el número de cuenta ${req.body.no_cuenta}`,
                from: 'No Reply <no_reply_skypunch@outlook.com>',
                to: `${req.body.nombre} <${req.body.email}>`,
                subject: 'Folio registrado'
              }, (err, message) => {
                res.status(200)
                .type('json')
                .json({
                  success: 'Ok',
                  body: payload || {},
                  email: err || message
                })
              })
            }
          })
          update.end()
        } else {
          res.status(400)
          .json({
            error: 'El boleto ya fue registrado'
          })
        }
      }
    })
    select.end()
  } else {
    res.status(415)
    .type('json')
    .json({
      error: 'Se esperaba una solicitud en formato JSON'
    })
  }
})
app.use((req, res, next) => {
  res.type('json')
  .status(404)
  .json({
    'error': 404,
    'mensaje': 'No Encontrado'
  })
})
app.use((err, req, res, next) => {
  console.error(err)
  res.type('json')
  .status(500)
  .json({
    'error': 500,
    'mensaje': 'Error en el servidor',
    'server': err
  })
})
app.listen(PORT, () => {
  console.log(`Aplicación corriendo en http://127.0.0.1:${PORT}`)
  // console.log(config)
})
